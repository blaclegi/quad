#include <Servo.h>

Servo myservo;
Servo myServo2;

int val = 0;
int prev;

int val2 = 0;
int prev2;

String getValue(String data, char separator, int index)
{
    int found = 0;
    int strIndex[] = { 0, -1 };
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == separator || i == maxIndex) {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i+1 : i;
        }
    }
    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  myservo.attach(3);
  myServo2.attach(2);
  myservo.write(val);
  myServo2.write(val2);
}

void loop() {

  // put your main code here, to run repeatedly:
  if(Serial.available()){
    String str = Serial.readStringUntil('\r\n');
    String xval = getValue(str, ':', 0);
    String yval = getValue(str, ':', 1);
    val = xval.toInt();
    val2 = yval.toInt();

    if (val != prev || val != 0){
      myservo.write(val);
      prev = val;
    }

    
    if (val2 != prev2 || val2 != 0){
      myServo2.write(val2);
      prev2 = val2;
    }
    
  }
  delay(1000/15);
  
}
