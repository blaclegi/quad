#include <SDL2/SDL.h> 
#include <iostream>
#include <stdio.h>

int main(int argc, char *argv[])
{
	SDL_Init(SDL_INIT_JOYSTICK);
	printf("%i joysticks were found.\n\n", SDL_NumJoysticks());
    printf("The names of the joysticks are:\n");
                
    for(int i = 0; i < SDL_NumJoysticks(); i++) {
		SDL_Joystick *joystick = SDL_JoystickOpen(i);
        printf("    %s\n", SDL_JoystickName(joystick));
    }

	SDL_Event e;
	bool quit = false;
	while(!quit) {
		while( SDL_PollEvent( &e ) != 0 ) {
			if( e.type == SDL_QUIT ) {
				quit = true;
			}
		}
	}
	return 0;
}