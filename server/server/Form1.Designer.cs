﻿namespace server
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this._pbLeftTrigger = new System.Windows.Forms.PictureBox();
            this._pbRightTrigger = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this._leftTriggerNum = new System.Windows.Forms.Label();
            this._rightTriggerNum = new System.Windows.Forms.Label();
            this._pbLeftThumb = new System.Windows.Forms.PictureBox();
            this._pbRightThumb = new System.Windows.Forms.PictureBox();
            this._lblLeftThumbPos = new System.Windows.Forms.Label();
            this._lblRightThumbPos = new System.Windows.Forms.Label();
            this._lblServoDbg = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this._pbLeftTrigger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._pbRightTrigger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._pbLeftThumb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._pbRightThumb)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Controller Connected:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(129, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Left Trigger";
            // 
            // _pbLeftTrigger
            // 
            this._pbLeftTrigger.Location = new System.Drawing.Point(86, 34);
            this._pbLeftTrigger.Name = "_pbLeftTrigger";
            this._pbLeftTrigger.Size = new System.Drawing.Size(255, 25);
            this._pbLeftTrigger.TabIndex = 3;
            this._pbLeftTrigger.TabStop = false;
            // 
            // _pbRightTrigger
            // 
            this._pbRightTrigger.Location = new System.Drawing.Point(86, 75);
            this._pbRightTrigger.Name = "_pbRightTrigger";
            this._pbRightTrigger.Size = new System.Drawing.Size(255, 25);
            this._pbRightTrigger.TabIndex = 5;
            this._pbRightTrigger.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Right Trigger";
            // 
            // _leftTriggerNum
            // 
            this._leftTriggerNum.AutoSize = true;
            this._leftTriggerNum.Location = new System.Drawing.Point(347, 39);
            this._leftTriggerNum.Name = "_leftTriggerNum";
            this._leftTriggerNum.Size = new System.Drawing.Size(0, 13);
            this._leftTriggerNum.TabIndex = 6;
            // 
            // _rightTriggerNum
            // 
            this._rightTriggerNum.AutoSize = true;
            this._rightTriggerNum.Location = new System.Drawing.Point(347, 80);
            this._rightTriggerNum.Name = "_rightTriggerNum";
            this._rightTriggerNum.Size = new System.Drawing.Size(0, 13);
            this._rightTriggerNum.TabIndex = 7;
            // 
            // _pbLeftThumb
            // 
            this._pbLeftThumb.Location = new System.Drawing.Point(16, 115);
            this._pbLeftThumb.Name = "_pbLeftThumb";
            this._pbLeftThumb.Size = new System.Drawing.Size(210, 210);
            this._pbLeftThumb.TabIndex = 8;
            this._pbLeftThumb.TabStop = false;
            // 
            // _pbRightThumb
            // 
            this._pbRightThumb.Location = new System.Drawing.Point(232, 115);
            this._pbRightThumb.Name = "_pbRightThumb";
            this._pbRightThumb.Size = new System.Drawing.Size(210, 210);
            this._pbRightThumb.TabIndex = 9;
            this._pbRightThumb.TabStop = false;
            // 
            // _lblLeftThumbPos
            // 
            this._lblLeftThumbPos.AutoSize = true;
            this._lblLeftThumbPos.Location = new System.Drawing.Point(16, 115);
            this._lblLeftThumbPos.Name = "_lblLeftThumbPos";
            this._lblLeftThumbPos.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._lblLeftThumbPos.Size = new System.Drawing.Size(0, 13);
            this._lblLeftThumbPos.TabIndex = 10;
            // 
            // _lblRightThumbPos
            // 
            this._lblRightThumbPos.AutoSize = true;
            this._lblRightThumbPos.Location = new System.Drawing.Point(232, 115);
            this._lblRightThumbPos.Name = "_lblRightThumbPos";
            this._lblRightThumbPos.Size = new System.Drawing.Size(0, 13);
            this._lblRightThumbPos.TabIndex = 11;
            // 
            // _lblServoDbg
            // 
            this._lblServoDbg.AutoSize = true;
            this._lblServoDbg.Location = new System.Drawing.Point(16, 327);
            this._lblServoDbg.Name = "_lblServoDbg";
            this._lblServoDbg.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._lblServoDbg.Size = new System.Drawing.Size(0, 13);
            this._lblServoDbg.TabIndex = 12;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 349);
            this.Controls.Add(this._lblRightThumbPos);
            this.Controls.Add(this._lblLeftThumbPos);
            this.Controls.Add(this._lblServoDbg);
            this.Controls.Add(this._pbRightThumb);
            this.Controls.Add(this._pbLeftThumb);
            this.Controls.Add(this._rightTriggerNum);
            this.Controls.Add(this._leftTriggerNum);
            this.Controls.Add(this._pbRightTrigger);
            this.Controls.Add(this.label4);
            this.Controls.Add(this._pbLeftTrigger);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this._pbLeftTrigger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._pbRightTrigger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._pbLeftThumb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._pbRightThumb)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox _pbLeftTrigger;
        private System.Windows.Forms.PictureBox _pbRightTrigger;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label _leftTriggerNum;
        private System.Windows.Forms.Label _rightTriggerNum;
        private System.Windows.Forms.PictureBox _pbLeftThumb;
        private System.Windows.Forms.PictureBox _pbRightThumb;
        private System.Windows.Forms.Label _lblLeftThumbPos;
        private System.Windows.Forms.Label _lblRightThumbPos;
        private System.Windows.Forms.Label _lblServoDbg;
    }
}

