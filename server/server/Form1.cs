﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using SharpDX.XInput;

namespace server
{
    public partial class Form1 : Form
    {
        private Object _lock = new object();
        private Controller _controller;
        private Thread _controllerPollThread;

        private Bitmap _leftTriggerIndicator = new Bitmap(255, 25);
        private Bitmap _rightTriggerIndicator = new Bitmap(255, 25);

        private Bitmap _leftThumbIndicator = new Bitmap(210, 210);
        private Bitmap _rightThumbIndicator = new Bitmap(210, 210);

        private SerialPort _serial;

        public Form1()
        {
            InitializeComponent();

            _controller = new Controller(UserIndex.One);
            label2.Text = _controller.IsConnected.ToString();

            _serial = new SerialPort("COM3", 115200);
            _serial.Open();
            _serial.ReadTimeout = SerialPort.InfiniteTimeout;
            _serial.WriteTimeout = SerialPort.InfiniteTimeout;

            _controllerPollThread = new Thread(ControllerThreadStart);
            _controllerPollThread.Start();
        }

        private PointF _leftLastThumbIndicatorPos = new PointF(0,0);
        private PointF _leftThumIndicatorPos = new PointF(0, 0);

        private PointF _rightLastThumbIndicatorPos = new PointF(0, 0);
        private PointF _rightThumbIndicatorPos = new PointF(0, 0);

        private void ControllerThreadStart()
        {
            while(true)
            {
                lock(_lock)
                {
                    Gamepad gamePad = _controller.GetState().Gamepad;

                    float deadband = 2500;
                    float lx = (Math.Abs((float)gamePad.LeftThumbX) < deadband) ? 0 : (float)gamePad.LeftThumbX / short.MinValue * -100;
                    float ly = (Math.Abs((float)gamePad.LeftThumbY) < deadband) ? 0 : (float)gamePad.LeftThumbY / short.MaxValue * 100;
                    float rx = (Math.Abs((float)gamePad.RightThumbX) < deadband) ? 0 : (float)gamePad.RightThumbX / short.MaxValue * 100;
                    float ry = (Math.Abs((float)gamePad.RightThumbY) < deadband) ? 0 : (float)gamePad.RightThumbY / short.MaxValue * 100;

                    _pbLeftThumb.Invoke((MethodInvoker)delegate () {
                        Graphics g = Graphics.FromImage(_leftThumbIndicator);
                        
                        _leftThumIndicatorPos.X += (lx - _leftLastThumbIndicatorPos.X);
                        _leftThumIndicatorPos.Y += (ly - _leftLastThumbIndicatorPos.Y);

                        g.Clear(Color.Black);
                        g.FillRectangle(Brushes.Red, _leftThumIndicatorPos.X + 100, -_leftThumIndicatorPos.Y + 100, 10,10);
                        _pbLeftThumb.Image = _leftThumbIndicator;
                        g.Dispose();
                    });

                    _pbRightThumb.Invoke((MethodInvoker)delegate () {
                        Graphics g = Graphics.FromImage(_rightThumbIndicator);

                        _rightThumbIndicatorPos.X += (rx - _rightLastThumbIndicatorPos.X);
                        _rightThumbIndicatorPos.Y += (ry - _rightLastThumbIndicatorPos.Y);

                        g.Clear(Color.Black);
                        g.FillRectangle(Brushes.Red, _rightThumbIndicatorPos.X + 100, -_rightThumbIndicatorPos.Y + 100, 10, 10);
                        _pbRightThumb.Image = _rightThumbIndicator;
                        g.Dispose();
                    });

                    float lyPercent = ((-ly + 100.0f) / 200) * 100;
                    float lyServoRange = (lyPercent / 100.0f) * 180.0f;

                    float rxPercent = ((-rx + 100.0f) / 200) * 100;
                    float rxServoRange = (rxPercent / 100.0f) * 180.0f;

                    /*if (!_serial.IsOpen)
                    {
                        MessageBox.Show("Error! The serial port has closed you twonk. " + , "Aha! Got you!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Invoke((MethodInvoker)delegate () {
                            Close();
                        });
                    }*/
                    _serial.WriteLine(Convert.ToInt32(lyServoRange).ToString() + ":" + Convert.ToInt32(rxServoRange).ToString());
                    
                    
                    _lblServoDbg.Invoke((MethodInvoker)delegate () {
                        _lblServoDbg.Text = Convert.ToInt32(lyServoRange).ToString();
                    });

                    _lblLeftThumbPos.Invoke((MethodInvoker)delegate () {
                        _lblLeftThumbPos.Text = lx.ToString("F1") + ", " + ly.ToString("F1");
                    });

                    _lblRightThumbPos.Invoke((MethodInvoker)delegate () {
                        _lblRightThumbPos.Text = rx.ToString("F1") + ", " + ry.ToString("F1");
                    });

                    _leftLastThumbIndicatorPos = new PointF(lx, ly);
                    _rightLastThumbIndicatorPos = new PointF(rx, ry);

                    _leftTriggerNum.Invoke((MethodInvoker)delegate () {
                        _leftTriggerNum.Text = gamePad.LeftTrigger.ToString();
                    });

                    _rightTriggerNum.Invoke((MethodInvoker)delegate () {
                        _rightTriggerNum.Text = gamePad.RightTrigger.ToString();
                    });

                    _pbLeftTrigger.Invoke((MethodInvoker)delegate () {
                        Graphics g = Graphics.FromImage(_leftTriggerIndicator);
                        g.Clear(Color.Black);
                        g.FillRectangle(Brushes.Red, 0, 0, gamePad.LeftTrigger, 25);
                        _pbLeftTrigger.Image = _leftTriggerIndicator;
                        g.Dispose();
                    });

                    _pbRightTrigger.Invoke((MethodInvoker)delegate () {
                        Graphics g = Graphics.FromImage(_rightTriggerIndicator);
                        g.Clear(Color.Black);
                        g.FillRectangle(Brushes.Red, 0, 0, gamePad.RightTrigger, 25);
                        _pbRightTrigger.Image = _rightTriggerIndicator;
                        g.Dispose();
                    });
                }
                Thread.Sleep(1000/5);
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            _serial.Close();
            _controllerPollThread.Abort();
        }
    }
}
